<!--floatval-->
<?php
$var = "10.92bitm";
$float_value_of_var = floatval($var);
echo $float_value_of_var; // 10.92
echo "<br>"
?>

<?php
$var = 'bitm10.92';
$float_value_of_var = floatval($var);
echo $float_value_of_var; // 0
echo "<br>"
?>
<!--vardump-->

<?php
$a = ("bitm b35");
var_dump($a);
echo "<br>";
?>
<!--empty-->
<?php
$var = 0;

// Evaluates to true because $var is empty
if (empty($var)) {
    echo '$var is either 0, empty, or not set at all';
}
echo "<br>";
?>

<!--is_array-->
<?php

$is_array_test="Not an array";
    if (is_array($is_array_test)){
echo "Result negative";
echo "\n";
}
$is_array_test=array("An","array");
if (is_array($is_array_test)){
    echo "Result positive";
    echo "<br>";
}
?>
<!--isset-->
<?php
$a=10;
$b=0;
if(isset($a)){
    echo "value of a is set";
    echo "<br>";
}
if(isset($b)){
    echo "value of b is set";
    echo "<br>";
}?>

<!--print_r-->

<?php
$a = array ("apple","banana");
print_r ($a);
echo "<br>"
?>
<!--serialize: tells the structure of the data-->

<?php
$serialized_data = serialize(array('Math', 'Language', 'Science'));
print_r($serialized_data);
echo "<br>";

$serialized_data_int = serialize(array('05', '24', '1'));
print_r($serialized_data_int);
echo "<br>";
$var1 = unserialize($serialized_data);
// Show the unserialized data;
var_dump ($var1);
echo "<br>";
?>
<!--unset: unset() destroys the specified variables.-->
<?php
$unset_test=31;
echo 'Before using unset() the value of $xys is : '.$unset_test."<br>";
unset($unset_test); //unset_test destroyed
?>

<!--var_export() gets structured information about the given variable.
It is similar to var_dump() with one exception: the returned representation is valid PHP code-->

<?php
$a = array ("a", "b", "c");
var_export($a);
echo "<br>";
?>

<!--is_scalar : checks whether the value is scaler or not... ret type-boolean-->
<?php

$var1 = "bitm";
if (is_scalar ($var1))
    echo $var1. ' is a scalar value'.'<br>';
else
    echo $var1.' is not a  scalar value'.'<br>';

$var2 = array("a","b","c");

if (is_scalar ($var2))
    echo "var 2 is a scalar value"."<br>";
else
    echo "var 2 is not a  scalar value"."<br>";
?>
